<?php
class ControllerMultimerchPaymentPrivatbank extends ControllerMultimerchBase {
	private $version = '1.0';
	private $name = 'privatbank';
	private $_log;

	private $settings = Array(
		'merchant_id' => '',	// merchant id
		'password' => '',	// merchant password
		'fee_enabled' => '',	// Enable gateway for fee payments
		'payout_enabled' => '',	// Enable gateway for payouts
		'debug' => 0
	);

	private $seller_settings = array(
		'card_number'
	);

	private $error = array();

	public function __construct($registry) {
		parent::__construct($registry);
		$this->registry = $registry;
		$this->data = array_merge($this->data, $this->load->language('multimerch/payment/' . $this->name));

		$this->load->model('setting/setting');

		$this->data['token'] = $this->session->data['token'];
		$this->data['name'] = $this->name;
		$this->data['admin_setting_full_prefix'] = MsPgPayment::ADMIN_SETTING_PREFIX . $this->name;
		$this->data['seller_setting_full_prefix'] = MsPgPayment::SELLER_SETTING_PREFIX . $this->name;

		if ($this->config->get($this->data['admin_setting_full_prefix'] . '_log_filename')){
			$this->_log = new Log($this->config->get($this->data['admin_setting_full_prefix'] . '_log_filename'));
		}else{
			$this->_log = new Log("pg_privatbank.log");
		}
	}

	public function jxGetPaymentForm() {
		if(empty($this->request->post) || !isset($this->request->post['pg_code']) || $this->request->post['pg_code'] !== $this->name) {
			$this->data['errors'][] = $this->language->get('ms_pg_payment_error_no_method');
		}


		if (empty($this->data['errors'])) {
			$request_ids = array_unique($this->request->post['request_ids']);
			$total_amount = 0;
			$payment_description = array();

			foreach ($request_ids as $request_id) {
				$request_info = MsLoader::getInstance()->MsPgRequest->getRequests(array('request_id' => $request_id, 'single' => 1));
				$valid_currencies = array('UAH','EUR','USD');
				if (!in_array($request_info["currency_code"],$valid_currencies)){
					$this->data['errors'][] = sprintf($this->data['error_currency'], $request_id);
				}

				$seller_id = $request_info['seller_id'];
				if($seller_id) {
					$seller_info = MsLoader::getInstance()->MsSeller->getSeller($seller_id);

					if (!$seller_info) continue;

					$seller_settings = MsLoader::getInstance()->MsSetting->getSellerSettings(
						array(
							'seller_id' => $seller_id,
							'code' => $this->data['seller_setting_full_prefix']
						)
					);

					if(empty($seller_settings) || !$this->validateSellerSettings($seller_settings)) {
						$this->data['errors'][] = sprintf($this->data['error_seller_info'], $this->url->link('multimerch/seller/update', 'token=' . $this->session->data['token'] . '&seller_id=' . $seller_id), $seller_info['ms.nickname']);
						continue;
					}

					foreach ($seller_settings as $key => $value) {
						$this->data['sellers'][$seller_id][str_replace(MsPgPayment::SELLER_SETTING_PREFIX . $this->name . '_', '', $key)] = $value;
					}

					$this->data['sellers'][$seller_id]['nickname'] = $seller_info['ms.nickname'];

					$total_amount += $request_info['amount'];
					$payment_description[$request_id] = $request_info['description'];

//					$this->data['sellers'][$seller_id]['amount'] = $this->MsLoader->MsBalance->getSellerBalance($seller_id) - $this->MsLoader->MsBalance->getReservedSellerFunds($seller_id);
					$this->data['sellers'][$seller_id]['request_id'] = $request_info['request_id'];
					$this->data['sellers'][$seller_id]['amount'] = $request_info['amount'];
					$this->data['sellers'][$seller_id]['amount_formatted'] = $this->currency->format(abs($request_info['amount']), $this->config->get('config_currency'));
				}
			}

			$this->data['total_amount'] = $total_amount;
			$this->data['total_amount_formatted'] = $this->currency->format($total_amount, $this->config->get('config_currency'));
			$this->data['payment_description'] = htmlspecialchars(json_encode($payment_description));
		}

		$this->response->setOutput(json_encode($this->load->view('multimerch/payment/' . $this->name . '_payment_form.tpl', $this->data)));
	}

	public function jxCompletePayment() {
		$json = array();
		$data = $this->request->post;

		if($this->config->get($this->data['admin_setting_full_prefix'] . '_debug'))
			$this->_log->write('Starting Admin Privatbank payment flow...');

		if(!isset($data['payment_method']) || !isset($data['payment_type']) || !isset($data['total_amount']) || !isset($data['payment_description'])) {
			$json['errors'][] = $this->language->get('ms_pg_payment_error_payment');

			if($this->config->get($this->data['admin_setting_full_prefix'] . '_debug'))
				$this->_log->write($this->language->get('ms_pg_payment_error_payment'));
		}

		if (!$this->config->get($this->data['admin_setting_full_prefix'] . '_merchant_id') OR !$this->config->get($this->data['admin_setting_full_prefix'] . '_password')){
			$json['errors'][] = sprintf($this->data['error_admin_info'], $this->url->link('multimerch/payment/' . $this->name, 'token=' . $this->session->data['token'], 'SSL'));
			if($this->config->get($this->data['admin_setting_full_prefix'] . '_debug'))
				$this->_log->write('admin settings error');
		}

		if(!isset($json['errors'])) {
			foreach ($data['receiver_data'] as $receiver_id => $receiver_data) {
				$request_info = MsLoader::getInstance()->MsPgRequest->getRequests(array('request_id' => $receiver_data["request_id"], 'single' => 1));
				$valid_currencies = array('UAH','EUR','USD');
				if (!in_array($request_info["currency_code"],$valid_currencies)){
					$json['errors'][] = sprintf($this->data['error_currency'], $receiver_data["request_id"]);
					if($this->config->get($this->data['admin_setting_full_prefix'] . '_debug'))
						sprintf($this->data['error_currency'], $receiver_data["request_id"]);
					break;
				}
				$payment_response = $this->makePbRequest($receiver_data,$request_info["currency_code"]);
				$seller_info = $this->MsLoader->MsSeller->getSeller($receiver_id);
				if (isset($payment_response->data->error)) {
					foreach ($payment_response->data->error as $error) {
						$json['errors'][] = (string)$error['message'];
						if ($this->config->get($this->data['admin_setting_full_prefix'] . '_debug'))
							$this->_log->write((string)$error['message']);
					}
					break;
				}
				if (isset($payment_response->data->payment['state']) AND (int)$payment_response->data->payment['state'] == 0){
					$json['errors'][] = sprintf($this->data['error_seller_card'], $this->url->link('multimerch/seller/update', 'token=' . $this->session->data['token'] . '&seller_id=' . $receiver_id), $seller_info['ms.nickname']) . ' ' . (string)$payment_response->data->payment['message'];
					if ($this->config->get($this->data['admin_setting_full_prefix'] . '_debug'))
						$this->_log->write('Reciever data error: '.(string)$payment_response->data->payment['message']);
					break;
				}
				if (isset($payment_response->data->payment['state']) AND (int)$payment_response->data->payment['state'] == 1){
					if ($this->config->get($this->data['admin_setting_full_prefix'] . '_debug'))
						$this->_log->write('Create payment....');
					if (isset($payment_response->data->payment['ref'])){
						$receiver_data['ref'] = $payment_response->data->payment['ref'];
					}
					$payment_id = $this->MsLoader->MsPgPayment->createPayment(array(
						'seller_id' => MsPgPayment::ADMIN_ID,
						'payment_type' => $data['payment_type'],
						'payment_code' => $data['payment_method'],
						'payment_status' => MsPgPayment::STATUS_INCOMPLETE,
						'amount' => $data['total_amount'],
						'currency_id' => $this->currency->getId($request_info["currency_code"]),
						'currency_code' => $request_info["currency_code"],
						'sender_data' => array('merchant_id' => $this->config->get($this->data['admin_setting_full_prefix'] . '_merchant_id')),
						'receiver_data' => $receiver_data,
						'description' => html_entity_decode($data['payment_description'])
					));
					if($payment_id) {
						if($this->config->get($this->data['admin_setting_full_prefix'] . '_debug'))
							$this->_log->write('Payment #' . $payment_id . ' was successfully created');

						// Bind payment request with payment id
						$this->MsLoader->MsPgRequest->updateRequest(
							$receiver_data['request_id'],
							array(
								'payment_id' => $payment_id,
								//'request_status' => MsPgRequest::STATUS_PAID,
								'request_status' => MsPgRequest::STATUS_UNPAID,
								'date_modified' => 1
							)
						);

						if($this->config->get($this->data['admin_setting_full_prefix'] . '_debug'))
						$this->_log->write('MM Payment Request # ' . $receiver_data['request_id'] . ' for seller # ' . $receiver_id . ' was successfully updated');

					}

//					$this->MsLoader->MsBalance->addBalanceEntry(
//						$seller_id,
//						array(
//							'withdrawal_id' => $payment_id,
//							'balance_type' => MsBalance::MS_BALANCE_TYPE_WITHDRAWAL,
//							'amount' => -$seller_data['amount'],
//							'description' => sprintf($this->language->get('ms_payment_royalty_payout'), $seller['name'], $this->config->get('config_name'))
//						)
//					);

				}
			}
		}
		return $this->response->setOutput(json_encode($json));
	}

	public function jxGetPgSettingsForm() {
		// Get seller settings
		foreach ($this->seller_settings as $setting_name) {
			if (isset($this->request->get['seller_id'])) {
				$setting_data = MsLoader::getInstance()->MsSetting->getSellerSettings(
					array(
						'seller_id' => $this->request->get['seller_id'],
						'name' => $this->data['seller_setting_full_prefix'] . '_' . $setting_name,
						'single' => 1
					)
				);
			}

			$this->data[$setting_name] = !empty($setting_data) ? $setting_data : '';
		}

		return $this->load->view('multimerch/payment/' . $this->name . '_settings_form.tpl', $this->data);
	}

	public function index() {
		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateAccess()) {
			$settings = array();
			foreach ($this->request->post as $key => $value) {
				$settings[$this->data['admin_setting_full_prefix'] . '_' . $key] = $value;
			}
			$this->model_setting_setting->editSetting($this->data['admin_setting_full_prefix'], $settings);
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('multimerch/payment-gateway', 'token=' . $this->session->data['token'], true));
		}

		$this->data['error_warning'] = isset($this->error['warning']) ? $this->error['warning'] : '';

		$this->data['breadcrumbs'] = MsLoader::getInstance()->MsHelper->admSetBreadcrumbs(array(
			array(
				'text' => $this->language->get('ms_menu_multiseller'),
				'href' => $this->url->link('module/multimerch', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('ms_pg_heading'),
				'href' => $this->url->link('multimerch/payment-gateway', '', 'SSL'),
			),
			array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('multimerch/payment/' . $this->name, '', 'SSL'),
			)
		));

		$this->data['action'] = $this->url->link('multimerch/payment/' . $this->name, 'token=' . $this->session->data['token'], true);
		$this->data['cancel'] = $this->url->link('multimerch/payment-gateway', 'token=' . $this->session->data['token'], true);

		//default log filename
		$this->settings['log_filename'] = 'pg_privatbank.'.uniqid().'.log';

		foreach ($this->settings as $setting_name => $value) {
			if (isset($this->request->post[$setting_name])) {
				$this->data[$setting_name] = $this->request->post[$setting_name];
			} else if($this->config->get($this->data['admin_setting_full_prefix'] . '_' . $setting_name )!== null) {
				$this->data[$setting_name] = $this->config->get($this->data['admin_setting_full_prefix'] . '_' . $setting_name);
			}else{
				$this->data[$setting_name] = $value;
			}
		}

		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('multimerch/payment/' . $this->name . '.tpl', $this->data));
	}

	protected function validateAccess() {
		if (!$this->user->hasPermission('modify', 'multimerch/payment/' . $this->name)) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}


	protected function validateSellerSettings($data) {
		$errors = array();
		foreach ($this->seller_settings as $setting_name) {
			if(!$data[$this->data['seller_setting_full_prefix'] . '_' . $setting_name]) {
				$errors[] = 'Error ' . $setting_name;
			}
		}
		return !$errors;
	}

	protected function makePbRequest($receiver_data,$currency_code) {

		$merchant_id = $this->config->get($this->data['admin_setting_full_prefix'] . '_merchant_id');
		$password = $this->config->get($this->data['admin_setting_full_prefix'] . '_password');

		$data  = '<oper>cmt</oper>';
		$data .= '<wait>90</wait>';
		$data .= '<test>0</test>';
		$data .= '<payment id="">';
		$data .= '<prop name="b_card_or_acc" value="'.$receiver_data['card_number'].'" />';
		$data .= '<prop name="amt" value="'.$receiver_data['amount'].'" />';
		$data .= '<prop name="ccy" value="'.$currency_code.'" />';
		$data .= '<prop name="details" value="request_id:'.$receiver_data['request_id'].'" />';
		$data .= '</payment>';

		$sign=sha1(md5($data.$password));

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
			<request version="1.0">
				 <merchant>
				   <id>'.$merchant_id.'</id>
				   <signature>'.$sign.'</signature>
				 </merchant>
				 <data>' .$data. '</data>
			</request>';


		if($this->config->get($this->data['admin_setting_full_prefix'] . '_debug'))
			$this->_log->write('PB request xml: '  . print_r($xml, true));

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,            "https://api.privatbank.ua/p24api/pay_pb" );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($ch, CURLOPT_POST,           1 );
		curl_setopt($ch, CURLOPT_POSTFIELDS,     $xml );
		curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/plain'));

		$payment_response = new SimpleXMLElement(curl_exec($ch));
		curl_close($ch);

		if($this->config->get($this->data['admin_setting_full_prefix'] . '_debug'))
			$this->_log->write('PB response: '  . print_r($payment_response, true));

		return $payment_response;
	}

}