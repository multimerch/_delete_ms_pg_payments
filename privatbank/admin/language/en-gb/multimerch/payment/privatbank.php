<?php
// Heading
$_['heading_title'] 		  = 'Privatbank';

// Text
$_['text_method_name'] 		  = 'Privatbank';

$_['text_success']			  = 'Success: You have modified [MultiMerch] Privatbank details.';
$_['text_edit']               = 'Edit [MultiMerch] Privatbank';
$_['text_privatbank']	  		= '<img src="/image/payment/privat24.png" alt="Privatbank" title="Privatbank" />';

$_['text_receiver'] 		  = 'Receiver';
$_['text_sender'] 			  = 'Sender';

$_['text_merchant_id'] 			  = 'Merchant ID';
$_['text_password'] 			  = 'Password';
$_['text_card_number'] 			  = 'Card number';


// Error
$_['error_permission']		  = 'Warning: You do not have permission to modify [MultiMerch] Privatbank!';
$_['error_no_payment_id']	  = 'Error: Unable to confirm Bank Privatbank payment!';
$_['error_currency']	  	= 'Error: Invoice #%s currency not valid!';


$_['error_admin_info'] 		  = 'You have to <a href="%s">specify settings</a> for this payment method.';
$_['error_seller_info'] 	  = '<a href="%s">%s</a> had not specified settings for this payment method and will not be paid.';
$_['error_seller_card'] 	  = '<a href="%s">%s</a> - seller card is not valid.';
?>