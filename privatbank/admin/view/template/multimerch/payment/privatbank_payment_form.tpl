<div class="alert alert-danger <?php echo isset($errors) ? '' : 'hidden'; ?>" style="position: relative;">
	<?php if(isset($errors)) { ?>
		<ul>
			<?php foreach($errors as $error) { ?>
				<li><?php echo $error; ?></li>
			<?php } ?>
		</ul>
	<?php } ?>
	<button type="button" class="close" data-dismiss="alert" style="position:absolute; top: 1px; right: 5px;">&times;</button>
</div>

<?php if(isset($sellers) && is_array($sellers)) { ?>
	<div class="receiver">
		<legend><?php echo $text_receiver; ?></legend>
		<?php foreach($sellers as $seller_id => $seller_data) { ?>
			<div class="card_info">
				<input type="hidden" name="receiver_data[<?php echo $seller_id; ?>][request_id]" value="<?php echo $seller_data['request_id']; ?>" />
				<input type="hidden" name="receiver_data[<?php echo $seller_id; ?>][amount]" value="<?php echo $seller_data['amount']; ?>" />

				<h4><?php echo $seller_data['nickname']; ?>: <?php echo $seller_data['amount_formatted']; ?></h4>
				<div class="row">
					<input type="hidden" name="receiver_data[<?php echo $seller_id; ?>][card_number]" value="<?php echo $seller_data['card_number']; ?>" />
					<div class="col-sm-5">
						<strong><?php echo $text_card_number; ?>: </strong>
					</div>
					<div class="col-sm-7">
						<p><?php echo $seller_data['card_number']; ?></p>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
<?php } ?>

<div class="buttons">
	<div class="pull-right">
		<p><strong><?php echo 'Total: ' . $total_amount_formatted; ?></strong></p>
		<input type="hidden" name="total_amount" value="<?php echo $total_amount; ?>" />
		<input type="hidden" name="payment_description" value="<?php echo $payment_description; ?>" />

		<button id="button-save" data-toggle="tooltip" title="<?php echo $ms_button_pay; ?>" class="btn btn-primary"><i class="fa fa-money"></i> <?php echo $ms_button_pay; ?></button>
	</div>
</div>

<script>
	$(function() {
		$("#button-save").click(function(e) {
			e.preventDefault();
			$('.methods-info .alert-danger').hide();
			var data = $(this).closest('form').serialize();
			$.ajax({
				type: "POST",
				dataType: "json",
				url: 'index.php?route=multimerch/payment/privatbank/jxCompletePayment&token=' + msGlobals.token,
				data: data,
				beforeSend: function() {
					$('#button-save').button('loading');
				},
				complete: function() {
					$('#button-save').button('reset');
				},
                success: function(jsonData) {
					if (!jQuery.isEmptyObject(jsonData.errors)) {
						for (i in jsonData.errors) {
							$('.methods-info .alert-danger').show().removeClass('hidden').html(' ' + jsonData.errors[i]);
						}
					} else {
						window.location = 'index.php?route=multimerch/payment&token=' + msGlobals.token;
					}
				}
			});
		});
	});
</script>