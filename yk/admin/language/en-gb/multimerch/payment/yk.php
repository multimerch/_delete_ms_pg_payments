<?php
// Heading
$_['heading_title'] 		  = 'Яндекс.Касса';

// Text
$_['text_method_name'] 		  = 'Яндекс.Касса';

$_['text_success']			  = 'Success: You have modified [MultiMerch] Яндекс.Касса details.';
$_['text_edit']               = 'Edit [MultiMerch] Яндекс.Касса';
$_['text_yk']	  			= '<img src="/image/payment/yk.png" alt="Яндекс.Касса" title="Яндекс.Касса" />';

$_['text_receiver'] 		  = 'Receiver';
$_['text_sender'] 			  = 'Sender';

$_['text_ym_number'] 			= 'Ваш кошелек Яндекс.Денег';
$_['text_ym_number_note'] 		= 'Номер кошелька Яндекс.Денег, на который будут поступать средства.';
$_['text_password'] 			= 'Shoppassword (Секретное слово)';
$_['text_password_note'] 		= 'Уникальный код, который знает только Yandex и Продавец. Используется интерфейсом инициализации оплаты.';

// Error
$_['error_permission']		  = 'Warning: You do not have permission to modify [MultiMerch] Яндекс.Касса!';
$_['error_no_payment_id']	  = 'Error: Unable to confirm Яндекс.Касса payment!';
$_['error_currency']	  	= 'Error: Invoice #%s currency not valid!';


$_['error_admin_info'] 		  = 'You have to <a href="%s">specify settings</a> for this payment method.';
$_['error_seller_info'] 	  = '<a href="%s">%s</a> had not specified settings for this payment method and will not be paid.';
$_['error_seller_card'] 	  = '<a href="%s">%s</a> - seller card is not valid.';
?>