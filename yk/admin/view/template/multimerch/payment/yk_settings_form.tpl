<div class="form-group">
    <label class="col-sm-2 control-label"><?php echo $text_card_number; ?></span></label>
    <div class="col-sm-10">
        <input type="text" name="payment_gateways[slr_pg_privatbank][card_number]" value="<?php echo $card_number; ?>" placeholder="<?php echo $text_card_number; ?>" class="form-control" />
    </div>
</div>