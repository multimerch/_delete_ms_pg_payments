<?php
// Text
$_['text_title'] 			= 'Privatbank';

$_['text_success'] 			= 'You have successfully modified Privatbank!';

$_['text_name'] 			= 'Name';
$_['text_card_number'] 		= 'Card number';

$_['text_full_name'] 		= '%s %s';
$_['text_receiver'] 		= 'Receiver';
$_['text_sender'] 			= 'Sender';

$_['button_save'] 			= 'Save';

$_['error_receiver_data'] 	= 'Sorry! No information provided for this payment method. Please, <a href="%s" target="_blank">contact</a> store owner for more information.';
$_['error_sender_data'] 	= 'You have to <a href="%s" target="_blank">specify</a> settings for this payment method!';

