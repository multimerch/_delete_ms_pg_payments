<form id="slr-pg-privatbank" class="ms-form form-horizontal">
    <input type="hidden" id="pg-name" value="<?php echo $pg_name; ?>">

    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $text_card_number; ?></span></label>
        <div class="col-sm-10">
            <input type="text" name="card_number" value="<?php echo $card_number; ?>" placeholder="<?php echo $text_card_number; ?>" class="form-control" />
        </div>
    </div>

    <div class="buttons">
        <div class="pull-right">
            <a class="btn btn-primary ms-pg-submit"><span><?php echo $button_save; ?></span></a>
        </div>
    </div>
</form>
